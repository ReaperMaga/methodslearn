package xyz.binarylolis;

/**
 * Copyright (c) ReaperMaga, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by ReaperMaga
 **/
public class Test {

    public static int summe(int x, int y) {
        int summe = x + y;
        return summe;
    }

}
